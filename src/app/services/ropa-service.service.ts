import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Ropa } from '../model/Ropa';

@Injectable({
  providedIn: 'root'
})
export class RopaServiceService {

  constructor(public afs:AngularFirestore) { }

  saveRopa(ropa : Ropa){//
    const refropa=this.afs.collection("ropa");
    if(ropa.uid==null)
    ropa.uid=this.afs.createId();
    refropa.doc(ropa.uid).set(Object.assign({},ropa),{merge:true})


  }

  
  getRopa(): Observable< any []>{
    return this.afs.collection("ropa").valueChanges();

  }

}
