import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearRopaPage } from './crear-ropa.page';

const routes: Routes = [
  {
    path: '',
    component: CrearRopaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearRopaPageRoutingModule {}
