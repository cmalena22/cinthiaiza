import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearRopaPage } from './crear-ropa.page';

describe('CrearRopaPage', () => {
  let component: CrearRopaPage;
  let fixture: ComponentFixture<CrearRopaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearRopaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearRopaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
