import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearRopaPageRoutingModule } from './crear-ropa-routing.module';

import { CrearRopaPage } from './crear-ropa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearRopaPageRoutingModule
  ],
  declarations: [CrearRopaPage]
})
export class CrearRopaPageModule {}
