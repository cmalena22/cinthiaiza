import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Ropa } from '../model/Ropa';
import { RopaServiceService } from '../services/ropa-service.service';

@Component({
  selector: 'app-crear-ropa',
  templateUrl: './crear-ropa.page.html',
  styleUrls: ['./crear-ropa.page.scss'],
})
export class CrearRopaPage implements OnInit {
  ropa: Ropa =new Ropa
  constructor(public router: Router,
    public ropaService:RopaServiceService,private route: ActivatedRoute) { }

  ngOnInit() {
  }
guardar(){
  console.log(this.ropa)
  this.ropaService.saveRopa(this.ropa);
  let navigationExtras: NavigationExtras={
  queryParams:{
    ropa:this.ropa
  }
};

//dirigise a otra pagina y pasarle los parametros
this.router.navigate(['/lista-ropa']);

}
}
