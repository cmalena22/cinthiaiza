import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaRopaPage } from './lista-ropa.page';

const routes: Routes = [
  {
    path: '',
    component: ListaRopaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaRopaPageRoutingModule {}
