import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaRopaPage } from './lista-ropa.page';

describe('ListaRopaPage', () => {
  let component: ListaRopaPage;
  let fixture: ComponentFixture<ListaRopaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRopaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaRopaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
