import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaRopaPageRoutingModule } from './lista-ropa-routing.module';

import { ListaRopaPage } from './lista-ropa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaRopaPageRoutingModule
  ],
  declarations: [ListaRopaPage]
})
export class ListaRopaPageModule {}
