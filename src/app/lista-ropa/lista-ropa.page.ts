import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RopaServiceService } from '../services/ropa-service.service';

@Component({
  selector: 'app-lista-ropa',
  templateUrl: './lista-ropa.page.html',
  styleUrls: ['./lista-ropa.page.scss'],
})
export class ListaRopaPage implements OnInit {
  ropa: Observable< any []>;
  constructor(public ropaService:RopaServiceService, public router:Router) { }

  ngOnInit() {
    this.ropa=this.ropaService.getRopa();

  }
  regresar(){
    this.router.navigate(['/crear-ropa']);
  }


}
