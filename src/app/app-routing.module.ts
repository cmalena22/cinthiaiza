import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'crear-ropa',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'crear-ropa',
    loadChildren: () => import('./crear-ropa/crear-ropa.module').then( m => m.CrearRopaPageModule)
  },
  {
    path: 'lista-ropa',
    loadChildren: () => import('./lista-ropa/lista-ropa.module').then( m => m.ListaRopaPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
